package com.devcamp.userorder_api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorder_api.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    List<COrder> findBycUserId(Long userId);
}
